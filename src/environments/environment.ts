const END_POINT = 'https://restcountries.eu/rest/v2/';
export const environment = {
  production: false,
  countries: END_POINT + 'all'
};

