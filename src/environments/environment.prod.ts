const END_POINT = 'https://restcountries.eu/rest/v2/';
export const environment = {
  production: true,
  countries: END_POINT + 'all'
};

