import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {

  constructor() { }

  createFavorite( item ){
    localStorage.setItem('favorites', JSON.stringify( item ) );
  }
  getFavorites(){
    return JSON.parse( localStorage.getItem( 'favorites' ) );
  }
}
