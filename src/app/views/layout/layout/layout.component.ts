import { Component, OnInit, TemplateRef } from '@angular/core';
import { CountriesService } from 'src/app/services/countries.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FavoritesService } from 'src/app/services/favorites.service';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  providers: [ CountriesService, FavoritesService ]
})
export class LayoutComponent implements OnInit {
  searchText;
  countrieData: any;
  countriesData = [];
  constructor(private countries: CountriesService, private modalService: NgbModal, public fav: FavoritesService) { }
  favorit = false;
  ngOnInit() {
    this.getCountries();
  }

  getCountries() {
    this.countries.getCountries().subscribe( countries => {
      this.mutableCountries( countries );
    });
  }

  mutableCountries( objects: any ) {
    const values = {};
    const region = objects;

    // tslint:disable-next-line: no-shadowed-variable
    region.map( ( region: object, index ) => {
      const key: string = region['region'];

      values[ key ] = values[ key ] ||  { continent: key, countries:[] };
      const { countries } = values[ key ];
      countries.push( { ...region, index } );
    });

    this.countriesData = Object.values( values );
  }

  open(content, countrie) {
    this.countrieData = countrie;
    this.validateFavorite();
    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
    }, (reason) => {
    });
  }

  validateFavorite() {
    const dbFav = this.fav.getFavorites();
    if (dbFav !== null) {
      const tmp = dbFav.find( item => item.name === this.countrieData.name );
      if ( tmp !== undefined  ) {
        this.favorit = true;
      } else {
        this.favorit = false;
      }
    } else {
      this.favorit = false;
    }
  }

  addFavorite( countrieData ) {
    const dbFav = this.fav.getFavorites();
    if (dbFav !== null) {
      const tmp = dbFav.find( item => item.name === countrieData.name );
      if ( tmp === undefined  ) {
        dbFav.push( countrieData );
        this.fav.createFavorite( dbFav );
        this. validateFavorite();
      }
    } else {
      const tmp = [];
      tmp.push( countrieData );
      this.fav.createFavorite( tmp );
      this. validateFavorite();
    }
  }

  removeFavorite( countrieData ){
    const dbFav = this.fav.getFavorites();
    const tmp = [];
    for (const key in dbFav) {
      if (Object.prototype.hasOwnProperty.call(dbFav, key)) {
        const element = dbFav[key];
        if ( element.name !== countrieData.name ) {
          tmp.push( element );
        }
      }
    }
    this.fav.createFavorite( tmp );
    this. validateFavorite();
  }


}
